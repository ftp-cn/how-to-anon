# some practical ways to help anonymize yourself

these are suggestions, and of course, not guarantees.

if anyone has better methods to accomplish this, contributions are welcome.

all security assumptions and opinions made here should be questioned, but this helps serve, at the very least, as a starting point.

any help to translate this document is also encouraged.

security and anonymity is a process. nothing is guaranteed, but it is possible to make it harder for authorities to prevent you from exercising the fundamental human right to dissent.

## device and platform

apple is preferable to android for a couple reasons.

apple is very strict about its process of manufacture. its leadership is based in a country that strongly values freedom of speech. they would be held accountable by the world for designing devices that compromise security for the sake of the chinese government. china is just one country of hundreds. they do not deserve special treatment.

even if apple makes special devices for china, security researchers should scrutinize everything about these devices. and if any single device is found to be compromised, we know now not to trust them.

for android devices, the only similar company is samsung. many similar claims can hopefully be made of samsung. google, however, is known to make compromises to the chinese government and is not trustworthy. avoid google products and services.

## phone service

it is best to purchase your phone service, if possible, pre-paid and with cash. don't use a service provider that knows your name, your home address, your form of payment, or anything else. starve them of your identity, starve them of data.

## cloud

turn off uploading data to the cloud. on apple this is called `icloud`.

on apple devices, you need an identity to download apps from the app store. this is a security concern, and when you are able to create your first 'anonymized' email address, you will want to change your `apple id`.

## network

it's important to never share your phone number. many services online will ask you to provide this because they might think an anonymous user might be illegitimate. DO NOT GIVE THEM YOUR PHONE NUMBER. use a different service instead. services like telegram, signal, twitter, facebook, any many others, either ask you for your phone number or REQUIRE it, and might disable your account if you refuse to provide one. get use to passing `captcha` screens, because many companies see anonymous browsing the same as activity done by spammers.

even if you're using a pre-paid phone number, if you register your account with that number and fail to pay for the service, your number might later be unusable, and any accounts tied to it will be inaccessible.

your online activity can also be linked to your identity by its `ip address` (a number used to identify your address to internet servers and other networks)

there are two secure ways to get an anonymized ip address to connect to servers with: tor and vpn.

the easiest way is to use tor. a good app for apple that is recommended by those who make tor can be found here: <https://apps.apple.com/us/app/onion-browser/id519296448>

the recommended tor client for android can be found here: <https://play.google.com/store/apps/details?id=org.torproject.android>

to use tor effectively, it's best to log into the web versions of the services (not in their app) using only the tor app.

the other way, vpn, might work better with built-in apps and services. the vpn has to be configured with the device and it should be checked regularly to be sure that it is still being used.

do not purchase access to a vpn with any government-issued currency. cryptocurrency is also problematic if they are purchased from someone else.

also, a vpn can log your traffic, so try to use one that you trust might not reveal your logs, and also, your 'exit' should be in a different, safer country.

a good vpn provider that accepts cryptocurrency is this [cryptostorm](http://stormwayszuh4juycoy4kwoww5gvcu2c4tdtpkup667pdwe4qenzwayd.onion/)

regardless of whether you use tor or vpn, always double-check to see your ip is not revealed by using a site like this: <https://ipleak.net/>

### network protocols

http is insecure (it does not encrypt what you send). it's better to use https, but that still reveals your ip address to potential spies. try to use tor (links that end with `.onion`) or ones that use https over vpn.

## identity

to start, you need a randomly-generated identity to use between services. anything you can think of or imagine has the possibility to reveal some part of your identity to anyone interested.

it's up to you whether you create a new random user identity between each service. if you re-use an identity, it becomes more 'pseudonymous' than anonymous, but it might also help for convenience.

to get a random identity that is unlikely be already used, visit this link:

<https://www.random.org/cgi-bin/randbyte?nbytes=16&format=h>

and then just remove the spaces, or replace the spaces with periods `.`, hyphens `-`, underscores `_`. these are usually safe substitutions for username delimiters.

copy this to a note that isn't uploaded to the cloud. keep this note around so you know where to start.

also, another thing that you could do to create an anonymous identity would be to use random words or phrases, such as the kind of 'seed phrase' one might use to encode a bitcoin wallet. you can create a random seed phrase here:

<https://bip32jp.github.io/english/>

a japanese version is also available.

be sure to click the button to generate a new phrase. in english, the default phrase is 'awake book subject'. this is not random, it is the default, so do not use that.

## email

next, it's very important that you get an email address you can use with other services. a good email service that encrypts your messages is [protonmail](https://protonirockerxow.onion/). however, they ask for another email address that is not another protonmail account before you can sign up. do not use your personal email address because that can be linked back to you, and certainly do not use a phone number.

your email is, essentially, for most services, the 'keys to the castle'-- it is used as a 'source of truth' for verifying your identity. so, it is very important you use one that not only is anonymous and not tied to any identifying information of yours (your phone number, your personal email, your credit card, or even your network ip address), and further, it is best to use a service that most others use so that you don't stand out to other automated systems as someone who is suspicious or doing something different, and finally, it is best if the email communication is encrypted in some way.

so, in order to get that, first make a new email account on a site like this: <http://danielas3rtn54uwmofdo3x2bsdifr47huasnmbgqzfrec5ubupvtpid.onion/mail/index.php>

1. click the register link, or click this one: <http://danielas3rtn54uwmofdo3x2bsdifr47huasnmbgqzfrec5ubupvtpid.onion/mail/postfixadmin/register.php>
2. give them your random identity, or make a new one here: <https://www.random.org/cgi-bin/randbyte?nbytes=16&format=h>
3. make a random passsword and copy and paste that into your notes. you can make some random passwords here: <https://www.random.org/passwords/?num=5&len=20&format=html&rnd=new>
4. register

they will offer a squirrelmail interface you can log into that ends in `@danwin1210.me`.

this email is fully anonymous. don't use it to sign up with online services, however, because they will see that as suspicious. sign up for a protonmail account (it is very popular and most services won't be suspicious of it, so you won't stand out). to register:

1. visit <https://protonirockerxow.onion/> in a tor browser
2. click 'create account'
3. select the 'free' plan. remember: never give fiat money to any service that might ask for it. any card that has your name on it is vulnerable. any phone number that is not pre-paid with cash is also vulnerable.
4. use your random id and leave it on `protonmail.com`.
5. use a different password (always use different passwords on each site, so that if one site is hacked, your password cannot be used on other sites)
6. provide a recovery email. this is important, since they will ask you for your phone number if you don't.
7. click the button to create the email.
8. they may require you to fill out a 'captcha' form, usually with images. as mentioned earlier, this can happen often, on many services.

they will email your recovery email for verification purposes. you will need to click the link they send you before you can use your anonymous email.

1. log into that interface with this link: <http://danielas3rtn54uwmofdo3x2bsdifr47huasnmbgqzfrec5ubupvtpid.onion/mail/squirrelmail/src/login.php>
2. your address should be your random id you made earlier, and it ends with `@danwin1210.me`
3. click the link and make sure it opens in your tor browser, and not to your device default browser.
4. you can now log in to protonmail. be sure to not provide them with any names that aren't randomly generated using the methods described earlier under the identity section.

## passwords

a good way to organize your passwords and logins is with bitwarden. ideally you would use a server running a copy of bitwarden for you. if that is not available, use their free service, provided here: <https://bitwarden.com/>

once you've registered with your anonymous email, take your passwords you've written to your notes and add them to bitwarden.

bitwarden is one place it might be safest to use something you will easily remember instead of writing it down. however, it should never be anything you've used before, you should never use it elsewhere, and you should not write it down unless it is someplace very safe. it could be a phrase should be innocuous and it should not be clear that it is used to access your bitwarden passwords. for everything else, let bitwarden generate passwords for you.

it's also important that as you use bitwarden for site logins, make sure that bitwarden knows which site is associated with each login by filling out the 'Website' field. this helps make it so that anyone trying to get your password by making a site that is not the same as the original, but might look similar, cannot get it pre-filled by your password manager. this is called a 'phishing' attack, and it is very common. always double-check what you're doing, and always maintain a healthy measure of skepticisim of all emails and communications.

## communication

signal, telegram, messenger, whatsapp, and other services that only allow you to log in using your email and/or using your real name, are unsafe.

one very good service that allows you to message others either in a group or individually is called `matrix`. a good client for that is [riot](https://about.riot.im/).

## search

sites will track your identity, even if they don't even know your name, for marketing purposes. it's best to use sites that say they won't do that, and one such site that offers search services is DuckDuckGo. try to use it instead of google. the DuckDuckGo tor `.onion` address is here: <https://3g2upl4pq6kufc4m.onion/>

## payments

the most private way to sent payments to others is using [monero](https://www.getmonero.org/). bitcoin and zcash are not 'private by default' so be careful using these services. especially if you purchase that currency with any currency provided by a government (known as `fiat currency`). the ideal way to obtain currency is to mine it with a device run on a vpn.

small amounts of monero (less than \$10 USD per month) can be mined using common hardware (desktop CPUs). also it is likely that others might donate `XMR` if asked.

it is best to only use a `full-node` monero wallet. this usually requires a laptop or desktop computer. otherwise, just use cash. avoid using a card.

## technical notes and suggestions

even if open source code is used in either apple or android devices, the apps are compiled and can contain features that can compromise your identity. it is difficult on these devices to ensure the code you see online and the code running on your device is the same.

ideally, a system similar to the linux app store is best. one that requires no 'log in' and all apps are signed, from open source code available online to the code running on your device.

tor on apple devices needs improvement, and because of this, the work being done here is important and needs to be finished: <https://github.com/iCepa>

also, there appears to be nothing like random.org on an .onion address.
